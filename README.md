# DBTracker

Androïd GPS Tracking App.

Application that uses Livetrack24 web-services to provide real-time geolocation.
Needs a livetrack24 account to run.

## Target ##

Min. Androïd version : Jelly Bean (SDK 16, Andrïd v. 4.1)

## Last release ##

NEW : Credentials management

## Documentation (FR) ##

http://www.bourles.ovh/index.php?page=blog#tracker