package dbourles.dbtracker;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import dbourles.services.GPSService;
import dbourles.services.IdTrackerFinder;
import dbourles.services.LoginService;
import dbourles.services.TrackCloser;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {
	public boolean isTracking = false;
	public int idSession;

    public void LiveTrack(View view) throws IOException {
    	GPSService gpsService = new GPSService(this);
    	Button btnTrack = (Button) findViewById(R.id.startStopButton);
    	EditText txtLogin = (EditText) findViewById(R.id.loginTextBox);
    	EditText txtPassword = (EditText) findViewById(R.id.passwordTextBox);
    	
    	String login = txtLogin.getText().toString();
    	String password = txtPassword.getText().toString();
    	
    	if(isTracking == false) {
        	try {
    			SaveLiveTrackCredentials(login, password);
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
        	
	    	IdTrackerFinder sid = new IdTrackerFinder();
	    	LoginService loginService = new LoginService();
	    	
	    	sid.execute(getString(R.string.apiIdURL), login, password);
	    	try {
	    		idSession = UserSessionId(sid.get());
	    		loginService.execute(getString(R.string.apiLoginURL), String.valueOf(idSession), login, password);
	    		gpsService.SetIdSession(idSession);
	    		gpsService.StartTracking();
	    	} catch(Exception exc) {
	    		btnTrack.setText(R.string.errorMessage);
	    	}
	    	
	    	btnTrack.setText(getString(R.string.trackingStarted));
	    	btnTrack.setBackgroundColor(Color.GREEN);
	    	btnTrack.setTextColor(Color.BLACK);
	    	isTracking = true;
    	} else {
    		TrackCloser closer = new TrackCloser();
    		
    		closer.execute(getString(R.string.apiEndTrackingURL), String.valueOf(idSession));
    		gpsService.StopTracking();
    		
    		btnTrack.setBackgroundColor(Color.RED);
    		btnTrack.setText(getString(R.string.trackingStopped));
    		btnTrack.setTextColor(Color.WHITE);
    		isTracking = false;
    	}
    }
    
    @Override
    public void onBackPressed() {
    	moveTaskToBack(true);
    }
    
    private void SaveLiveTrackCredentials(String login, String password) throws IOException {
    	FileOutputStream fosLogin = openFileOutput(getString(R.string.login), MainActivity.MODE_PRIVATE);
    	FileOutputStream fosPassword = openFileOutput(getString(R.string.password), MainActivity.MODE_PRIVATE);
    	fosLogin.write(login.getBytes());
    	fosPassword.write(password.getBytes());
    	fosLogin.close();
    	fosPassword.close();
    }
    
    private String GetSavedCredentials(String fileName) throws IOException {
    	FileInputStream fis = openFileInput(fileName);
    	StringBuilder builder = new StringBuilder();
    	
    	int ch;
    	while((ch = fis.read()) != -1){
    	    builder.append((char)ch);
    	}
    	return builder.toString();
    }
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
    	EditText txtLogin = (EditText) findViewById(R.id.loginTextBox);
    	EditText txtPassword = (EditText) findViewById(R.id.passwordTextBox);
    	
    	try {
			txtLogin.setText(GetSavedCredentials(getString(R.string.login)));
	    	txtPassword.setText(GetSavedCredentials(getString(R.string.password)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private int UserSessionId(int userId) {
		Random a = new Random( System.currentTimeMillis() );
		int rnd = a.nextInt();
		return rnd = ( rnd &  0x7F000000 ) | ( userId & 0x00ffffff) | 0x80000000; 
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		
		if (id == R.id.franckTracker) {
			Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.livetrack24.com/user/Franck74/2d"));
	       	startActivity(viewIntent);
			return true;
		} else if (id == R.id.domTracker) {
			Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.livetrack24.com/user/dom/2d"));
	       	startActivity(viewIntent);
			return true;
		} else if (id == R.id.ben2rTracker) {
			Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.livetrack24.com/user/benoit2R/2d"));
	       	startActivity(viewIntent);
			return true;
		} else if (id == R.id.benExpertTracker) {
			Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.livetrack24.com/user/benexpert/2d"));
	       	startActivity(viewIntent);
			return true;
		} else if (id == R.id.domTracker) {
			Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.livetrack24.com/user/Dom/2d"));
	       	startActivity(viewIntent);
			return true;
		} else if (id == R.id.florianTracker) {
			Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.livetrack24.com/user/peintel/2d"));
	       	startActivity(viewIntent);
			return true;
		} else if (id == R.id.yoannTracker) {
			Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.livetrack24.com/user/yobzh74/2d"));
	       	startActivity(viewIntent);
			return true;
		} else if (id == R.id.quitTracker) {
			System.exit(0);
			return true;
		}
		
		if(id == R.id.quitTracker){
			System.exit(0);
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
}
