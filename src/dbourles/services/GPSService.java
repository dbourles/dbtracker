package dbourles.services;
import dbourles.dbtracker.R;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class GPSService implements LocationListener {
	LocationManager locationManager;
	Activity activity;
	int idSession;
	
	public GPSService(Activity mainActivity) {
		LocationManager locMan = (LocationManager) mainActivity.getSystemService(Context.LOCATION_SERVICE);
		locationManager = locMan;
		activity = mainActivity;
	}
	
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}
	@Override
	public void onProviderEnabled(String provider) {
	}
	@Override
	public void onProviderDisabled(String provider) {
	}
	
	@Override
	public void onLocationChanged(Location location) {
		PositionSender gpsSender = new PositionSender();
		
		gpsSender.execute(activity.getString(R.string.apiPositionSendUrl), 
				String.valueOf(idSession), 
				String.valueOf(location.getLatitude()), 
				String.valueOf(location.getLongitude()), 
				String.valueOf(location.getAltitude()), 
				String.valueOf(location.getSpeed()), 
				String.valueOf(location.getBearing()), 
				String.valueOf(System.currentTimeMillis() / 1000L));
	}
	
	public void StartTracking () {
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 1, this);
		// locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
	}
	
	public void StopTracking() {
		locationManager.removeUpdates(this);
	}
	
	public void SetIdSession(int id) {
		idSession = id;
	}
}
