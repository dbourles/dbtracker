package dbourles.services;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import android.os.AsyncTask;

public class IdTrackerFinder extends AsyncTask<String, Void, Integer> {
	private HttpClient client = new DefaultHttpClient();
	
	@Override
	protected Integer doInBackground(String... params) {
		Integer sid = 0;
		
        try {
            String url = String.format(params[0], params[1], params[2]);        
        	HttpGet query = new HttpGet(url);
            HttpResponse response = client.execute(query);
            HttpEntity entity = response.getEntity();
            sid = Integer.valueOf(EntityUtils.toString(entity));
        } catch (Exception exc) { 
        	exc.toString(); 
        }	
        
	    return sid;
	}
}
