package dbourles.services;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;

public class PositionSender extends AsyncTask<String, Void, String> {
	private HttpClient client = new DefaultHttpClient();
	
	@Override
	protected String doInBackground(String... params) {
		String logStatus = "";
		
        try {
            String url = String.format(params[0], params[1],  params[2], params[3], 
            		params[4], params[5], params[6], params[7]);
            HttpGet query = new HttpGet(url);
            HttpResponse response = client.execute(query);
            HttpEntity entity = response.getEntity();
            logStatus = EntityUtils.toString(entity);
        } catch (Exception exc) { 
        	exc.toString(); 
        }	
        
	    return logStatus;
	}
/*
 * GPS POINT PACKET
/track.php?leolive=4&sid=42664778&pid=321&lat=22.3&lon=40.2&alt=23&sog=40&cog=160&tm=1241422845
/track.php?leolive=4&amp;sid=%1$s&amp;pid=321&amp;lat=%3$s&amp;lon=%4$s&amp;alt=%5$s&amp;sog=%6$s&amp;cog=%7$s&amp;tm=%8$s
PARAMETERS

leolive=4 // 4 means this is a gps point
lat=22.3 // the latitude in decimal notation, use negative numbers for west
lon=40.2 // lon in decimal, use negative numbers for south
alt=23 // altitude in meters above the MSL (not the geoid if it is possible) , no decimals
sog=40 // speed over ground in km/h no decimals
cog=160 // course over ground in degrees 0-360, no decimals
tm=1241422845 // the unixt timestamp in GMT of the GPS time, not the phone's time.
 */
}
