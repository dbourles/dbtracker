package dbourles.services;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;

public class TrackCloser extends AsyncTask<String, Void, String> {
	private HttpClient client = new DefaultHttpClient();

	@Override
	protected String doInBackground(String... params) {
		String logStatus = "";
		
        try {
            String url = String.format(params[0], params[1]);
            HttpGet query = new HttpGet(url);
            HttpResponse response = client.execute(query);
            HttpEntity entity = response.getEntity();
            logStatus = EntityUtils.toString(entity);
        } catch (Exception exc) { 
        	exc.toString(); 
        }	
        
	    return logStatus;
	}
}
